package com.iot.practice.util;

import cn.hutool.json.JSON;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

/**
 * <p>WxUtil 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2020年12月01日 14:39</p>
 * <p>@remark：</p>
 */
public class WxUtil {
    /**
     * @return access_token
     * @throws Exception
     */
    public static String postToken() throws Exception {
        //小程序id
        String apiKey = "wxfd9662bcba3e7450";
        //小程序密钥
        String secretKey = "e3bfba943f7e15193b9bd1626aa38178";
        String requestUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + apiKey + "&secret=" + secretKey;
        URL url = new URL(requestUrl);
        // 打开和URL之间的连接
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        // 设置通用的请求属性
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.setUseCaches(false);
        connection.setDoOutput(true);
        connection.setDoInput(true);

        // 得到请求的输出流对象
        DataOutputStream out = new DataOutputStream(connection.getOutputStream());
        out.writeBytes("");
        out.flush();
        out.close();

        // 建立实际的连接
        connection.connect();
        // 定义 BufferedReader输入流来读取URL的响应
        BufferedReader in = null;
        if (requestUrl.contains("nlp")) {
            in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "GBK"));
        }else {
            in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
        }
        String result = "";
        String getLine;
        while ((getLine = in.readLine()) != null) {
            result += getLine;
        }
        in.close();
        JSONObject jsonObject = JSONUtil.parseObj(result);
        String accesstoken = jsonObject.getStr("access_token");
        return accesstoken;
    }

    /**
     * 生成带参小程序二维码
     *
     * @param sceneStr    参数
     * @param accessToken token
     */
    public static Map<String, Object> getminiqrQr(String sceneStr, String accessToken) {
        Map<String, Object> retMap = null;
        try {
            URL url = new URL("https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=" + accessToken);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            // conn.setConnectTimeout(10000);//连接超时 单位毫秒
            // conn.setReadTimeout(2000);//读取超时 单位毫秒
            // 发送POST请求必须设置如下两行
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            // 提交模式
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-Type", "application/x-javascript; charset=UTF-8");
            // 获取URLConnection对象对应的输出流
            PrintWriter printWriter = new PrintWriter(httpURLConnection.getOutputStream());
            // 发送请求参数
            JSONObject paramJson = new JSONObject();
            paramJson.put("scene", sceneStr);
            paramJson.put("page", "pages/index/main");
            paramJson.put("width", 430);
            paramJson.put("auto_color", true);


            printWriter.write(paramJson.toString());
            // flush输出流的缓冲
            printWriter.flush();
            //开始获取数据
            BufferedInputStream bis = new BufferedInputStream(httpURLConnection.getInputStream());
            /*ByteArrayOutputStream os = new ByteArrayOutputStream();*/
            OutputStream os = new FileOutputStream(new File("G:/test/2.txt"));
            int len;
            byte[] arr = new byte[1024];
            while ((len = bis.read(arr)) != -1) {
                os.write(arr, 0, len);
                os.flush();
            }
            os.close();
//            // 上传到oss
//            String fileName = CurrentTimeMillisId.next() + ".jpg";
//            String urls = createFilePath(fileName, sourceType);
//            InputStream is = new ByteArrayInputStream(os.toByteArray());
//            String miniCodeUrl = ossFileService.uploadFile(is, urls);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retMap;
    }

    public static void main(String[] args) throws Exception {
        String accessToken = postToken();
        System.out.println("获取的accessToken:" + accessToken);
        System.out.println("G:/test/2.txt");
////        getminiqrQr("HelloApplet", accessToken);
////        getminiqrQr("2/17613/9216", accessToken);
////        getminiqrQr("1/17613/475", accessToken);
        getminiqrQr("1232", accessToken);

    }
}
