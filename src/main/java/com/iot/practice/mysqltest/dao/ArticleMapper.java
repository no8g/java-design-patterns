package com.iot.practice.mysqltest.dao;

import com.iot.practice.mysqltest.entity.Article;
import org.springframework.stereotype.Repository;

/**
 * <p>ArticleMapper 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年01月21日 11:20</p>
 * <p>@remark：</p>
 */
@Repository
public interface ArticleMapper {

    /**
     * 根据用户id查询文章
     *
     * @return 用户的文章
     */
    Article selectArticleByUserId();
}
