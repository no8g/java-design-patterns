package com.iot.practice.mysqltest.entity;

import lombok.Data;

import java.util.List;

/**
 * <p>User 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年01月21日 11:02</p>
 * <p>@remark：</p>
 */
@Data
public class User {
    /**
     * 主键
     */
    private String id;
    /**
     * 用户姓名
     */
    private String userName;
    /**
     * 类Article
     */
    private List<Article> articleList;
}
