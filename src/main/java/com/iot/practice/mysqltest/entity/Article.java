package com.iot.practice.mysqltest.entity;

import lombok.Data;

/**
 * <p>Article 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年01月21日 11:03</p>
 * <p>@remark：</p>
 */
@Data
public class Article {
    /**
     * 主键
     */
    private String id;
    /**
     * 文章标题
     */
    private String articleTitle;
    /**
     * 文章内容
     */
    private String articleContent;
}
