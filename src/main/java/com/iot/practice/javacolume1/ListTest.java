package com.iot.practice.javacolume1;

import cn.hutool.core.collection.CollectionUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>ListTest �������ڣ�</p>
 * <p>@author��hujm</p>
 * <p>@date��2021��01��14�� 11:46</p>
 * <p>@remark��</p>
 */
public class ListTest {

    public static void main(String[] args) {

        ArrayList<String> listA = CollectionUtil.toList("a", "b", "c", "d", "e");
        ArrayList<String> listB = CollectionUtil.toList( "b", "c", "d", "e", "f");
        // listA和listB交集
        List<String> intersection = listA.stream().filter(listB::contains).collect(Collectors.toList());
        System.out.println("listA和listB交集 = " + intersection);

        // listA和listB并集（不去重）
        listA.addAll(listB);
        System.out.println("listA和listB并集 = " + listA);

        // listA和listB并集（去重）
        List<String> distinctList = listA.stream().distinct().collect(Collectors.toList());
        System.out.println("listA和listB 去重并集 = " + distinctList);

        // listA和listB的差集（listA - listB）
        List<String> collect = listA.stream().filter(item -> !listB.contains(item)).collect(Collectors.toList());
        System.out.println("listA - listB的差集 = " + collect);

        // listA和listB的差集（listB - listA）
        List<String> collect1 = listB.stream().filter(item -> !listA.contains(item)).collect(Collectors.toList());
        System.out.println("listB - listA的差集 = " + collect1);

    }
}
