package com.iot.practice.javacolume1;

import cn.hutool.http.HtmlUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * <p>HtmlFilterTest 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2020年11月27日 17:35</p>
 * <p>@remark：</p>
 */
public class HtmlFilterTest {

    public static void main(String[] args) throws IOException {
        File file = new File("C:\\Users\\Administrator\\Desktop\\2.txt");
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        StringBuilder sb = new StringBuilder();

        String string = null;

        while (bufferedReader.readLine() != null) {
            sb.append(System.lineSeparator());
        }
        bufferedReader.close();

        String s = HtmlUtil.cleanHtmlTag(sb.toString());
        System.out.println(s);
    }
}
