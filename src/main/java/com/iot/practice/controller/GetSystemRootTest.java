package com.iot.practice.controller;

import java.util.Map;

/**
 * <p>GetSystemRootTest 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2020年11月23日 14:35</p>
 * <p>@remark：</p>
 */
public class GetSystemRootTest {

    public static void main(String[] args) {

        /*File[] roots = File.listRoots();
        for (int i = 0; i < roots.length; i++) {
            System.out.println(roots[i]);
        }*/

        Map<String, String> map = System.getenv();
        String userName = map.get("USERNAME");// 获取用户名
        String computerName = map.get("COMPUTERNAME");// 获取计算机名
        String userDomain = map.get("USERDOMAIN");// 获取计算机域名

        System.out.println("USERNAME ----> " + userName);
        System.out.println("computerName ----> " + computerName);
        System.out.println("userDomain ----> " + userName);
    }
}
