package com.iot.practice.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iot.practice.domain.dataobject.MainsiteQuoteDO;
import org.springframework.stereotype.Repository;

/**
 * <p>MainsiteQuoteMapper此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2020年09月25日 16:39</p>
 * <p>@remark：</p>
 */
@Repository
public interface MainsiteQuoteMapper extends BaseMapper<MainsiteQuoteDO> {
}
