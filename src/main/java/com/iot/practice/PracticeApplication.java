package com.iot.practice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * <p>PracticeApplication此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2020年09月25日 16:22</p>
 * <p>@remark：</p>
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class PracticeApplication {

    public static void main(String[] args) {
        SpringApplication.run(PracticeApplication.class, args);
    }
}
