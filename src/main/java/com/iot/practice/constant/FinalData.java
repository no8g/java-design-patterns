package com.iot.practice.constant;

/**
 * <p>FinalData此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2020年11月03日 15:46</p>
 * <p>@remark：</p>
 */
public class FinalData {

    public static void main(String[] args) {
        Thread thread = new Thread();
        Thread.State state = thread.getState();
        System.out.println("创建之后：" + state);
        thread.start();
        Thread.State stateStart = thread.getState();
        System.out.println("Start之后：" + stateStart);
        boolean alive = thread.isAlive();
        System.out.println("alive：" + alive);
        thread.interrupt();
        boolean aliveInterrupt = thread.isAlive();
        System.out.println("interrupt之后：" + aliveInterrupt);


    }
}
