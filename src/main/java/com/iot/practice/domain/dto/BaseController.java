package com.iot.practice.domain.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.iot.practice.domain.CurrentUserDetails;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>BaseController此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2020年11月04日 9:29</p>
 * <p>@remark：</p>
 */
public class BaseController {

    /**
     * 得到request对象
     *
     * @return request对象
     */
    public HttpServletRequest getRequest() {
        HttpServletRequest servletRequest = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return servletRequest;
    }

    /**
     * 获取当前登录用户
     *
     * @return
     */
    public CurrentUserDetails getUser() {
        CurrentUserDetails userDetails = (CurrentUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userDetails;
    }

    protected <T> Page<T> getPage() {

        Integer page = 1;
        Integer limit = 15;
        Page<T> pageDto = new Page<>(page, limit, true);
        return pageDto;
    }
}
