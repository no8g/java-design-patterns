package com.iot.practice.domain.dto;

import lombok.Data;

import java.util.List;

/**
 * <p>PageResult此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2020年11月04日 8:48</p>
 * <p>@remark：</p>
 */
@Data
public class PageResult<T> {

    /**
     * 状态码 0-成功，1-失败
     */
    private int code;

    /**
     * 提示信息
     */
    private String msg;

    /**
     * 总条数
     */
    private int total;

    /**
     * 返回的数据列表
     */
    private List<T> data;
}
