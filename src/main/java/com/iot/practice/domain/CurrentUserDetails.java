package com.iot.practice.domain;

import cn.hutool.json.JSONObject;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>CurrentUserDetails此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2020年11月04日 10:17</p>
 * <p>@remark：</p>
 */
@Data
public class CurrentUserDetails {

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 租户ID
     */
    private Long tenantId;
    /**
     * 用户钉钉ID
     */
    private String dingUserId;

    /**
     * 租户名称
     */
    private String tenantName;

    /**
     * 租户类型
     */
    private String tenantType;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 授权集合
     */
    private Collection<? extends GrantedAuthority> authorities;

    /**
     * 菜单
     */
    private JSONObject menus;


    /**
     * 用户权限编码集合
     */
    private List<String> privilegeCodeList;

    /**
     * 角色编码
     */
    private Set<String> roleCode = new HashSet<>();
    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 真实名字
     */
    private String trueName;

    /**
     * 用户sid
     */
    private String agentSid;

    /**
     * adminFid
     */
    private String agentFid;
    /**
     * 用户数据权限级别
     * 1 所有店面 2 当前店面 3当前用户
     */
    private String roleAuth;
    /**
     * 头像
     */
    private String headPortrait;
    /**
     * 部门或店面
     */
    private String department;

    /**
     * 当前登陆人sid是否匹配租户id
     */
    private boolean matchFlag;
}
