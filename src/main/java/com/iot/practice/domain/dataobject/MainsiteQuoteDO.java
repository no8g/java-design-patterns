package com.iot.practice.domain.dataobject;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>MainsiteQuoteDO此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2020年09月25日 16:34</p>
 * <p>@remark：</p>
 */
@Data
@TableName("mainsite_quote")
public class MainsiteQuoteDO {

    @TableId(value = "quoteId", type = IdType.AUTO)
    private Integer quoteId;
    private Integer siteId;
    private String siteName;
    private String city;
    private String area;
    private String customerName;
    private String customerPhone;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT + 8")
    private String createTime;
    private Integer status;
    private String searchKey;
    private String customerSource;
    private String customerMessage;
}
