package com.iot.practice.service.impl;

import com.iot.practice.dao.MainsiteQuoteMapper;
import com.iot.practice.domain.dataobject.MainsiteQuoteDO;
import com.iot.practice.service.MainsiteQuoteSerivce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>MainsiteQuoteSerivceImpl此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2020年09月25日 16:50</p>
 * <p>@remark：</p>
 */
@Service
public class MainsiteQuoteSerivceImpl implements MainsiteQuoteSerivce {

    @Autowired
    private MainsiteQuoteMapper mainsiteQuoteMapper;

    @Override
    public List<MainsiteQuoteDO> queryList() {
        return null;
    }

    @Override
    public MainsiteQuoteDO queryOneById(Integer quoteId) {
        MainsiteQuoteDO mainsiteQuoteDO = mainsiteQuoteMapper.selectById(quoteId);
        return mainsiteQuoteDO;
    }
}
