package com.iot.practice.service;

import com.iot.practice.domain.dataobject.MainsiteQuoteDO;

import java.util.List;

/**
 * <p>MainsiteQuoteSerivce此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2020年09月25日 16:50</p>
 * <p>@remark：</p>
 */
public interface MainsiteQuoteSerivce {

    List<MainsiteQuoteDO> queryList();

    MainsiteQuoteDO queryOneById(Integer quoteId);
}
