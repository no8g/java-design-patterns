package com.iot.practice.designpattern.command.pattern;

/**
 * <p>AddRequirementCommand 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月19日 15:38</p>
 * <p>@remark：增加一项需求</p>
 */
public class AddRequirementCommand extends Command {

    /**
     * 执行增加一项需求的命令
     */
    @Override
    public void execute() {
        // 找到需求组
        super.requirementGroup.find();

        // 增加一份需求
        super.requirementGroup.add();

        // 给出计划
        super.requirementGroup.plan();
    }
}
