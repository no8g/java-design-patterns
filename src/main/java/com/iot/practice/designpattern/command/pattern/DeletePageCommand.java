package com.iot.practice.designpattern.command.pattern;

/**
 * <p>DeleltePageCommand 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月19日 15:41</p>
 * <p>@remark：<删除一个页面的命令/p>
 */
public class DeletePageCommand extends Command {

    /**
     * 执行删除一个页面的命令
     */
    @Override
    public void execute() {
        // 找到页面组
        super.pageGroup.find();
        // 删除一个页面
        super.pageGroup.delete();
        // 给出计划
        super.pageGroup.plan();
    }
}
