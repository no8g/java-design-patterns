package com.iot.practice.designpattern.command.pattern;

/**
 * <p>Invoker 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月19日 15:42</p>
 * <p>@remark：接头人的职责就是接收命令，并执行</p>
 */
public class Invoker {

    /**
     * 什么命令
     */
    private Command command;

    /**
     * 客户发出命令
     */
    public void setCommand(Command command) {
        this.command = command;
    }

    /**
     * 执行客户的命令
     */
    public void action() {
        this.command.execute();
    }
}
