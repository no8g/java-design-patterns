package com.iot.practice.designpattern.command.pattern;

import com.iot.practice.designpattern.command.CodeGroup;
import com.iot.practice.designpattern.command.PageGroup;
import com.iot.practice.designpattern.command.RequirementGroup;

/**
 * <p>Command 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月19日 15:34</p>
 * <p>@remark：命令的抽象类，我们把客户发出的命令定义成一个一个的对象</p>
 */
public abstract class Command {

    /**
     * 把三个组都定义好，子类可以直接使用
     *
     * 需求组
     */
    protected RequirementGroup requirementGroup = new RequirementGroup();

    /**
     * 美工组
     */
    protected PageGroup pageGroup = new PageGroup();

    /**
     * 代码组
     */
    protected CodeGroup codeGroup = new CodeGroup();

    /**
     * 只要一个方法，你要我做什么事情
     */
    public abstract void execute();
}
