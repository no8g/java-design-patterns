package com.iot.practice.designpattern.command.pattern;

/**
 * <p>CommandClient 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月19日 15:45</p>
 * <p>@remark：</p>
 */
public class CommandClient {

    public static void main(String[] args) {
        // 定义我们的接头人
        Invoker zhangSan = new Invoker();

        // 客户要求增加一项需求
        System.out.println("\n-------------客户要求增加一项需求-----------------");

        // 客户给我们下命令来
        Command command = new AddRequirementCommand();

        // 接头人接收到命令
        zhangSan.setCommand(command);

        // 接头人执行命令
        zhangSan.action();


        // 客户要求增加一项需求
        System.out.println("\n-------------客户要求删除一个页面-----------------");
        // 客户给我们下命令来
        Command command1 = new DeletePageCommand();

        //接头人接收到命令
        zhangSan.setCommand(command1);

        //接头人执行命令
        zhangSan.action();
    }
}
