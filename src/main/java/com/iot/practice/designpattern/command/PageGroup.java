package com.iot.practice.designpattern.command;

/**
 * <p>PageGroup 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月19日 15:23</p>
 * <p>@remark：美工组的职责是设计出一套漂亮、简单、便捷的界面</p>
 */
public class PageGroup extends Group {

    @Override
    public void find() {
        System.out.println("找到美工组...");
    }

    @Override
    public void add() {
        System.out.println("客户要求增加一个页面...");
    }

    @Override
    public void delete() {
        System.out.println("客户要求删除一个页面...");
    }

    @Override
    public void change() {
        System.out.println("客户要求修改一个页面...");
    }

    @Override
    public void plan() {
        System.out.println("客户要求页面变更计划...");
    }
}
