package com.iot.practice.designpattern.command;

/**
 * <p>RequirementGroup 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月19日 15:14</p>
 * <p>@remark：</p>
 */
public class RequirementGroup extends Group {

    @Override
    public void find() {
        System.out.println("找到需求组...");
    }

    @Override
    public void add() {
        System.out.println("客户要求增加一项需求...");
    }

    @Override
    public void delete() {
        System.out.println("客户要求删除一项需求...");
    }

    @Override
    public void change() {
        System.out.println("客户要求修改一项需求...");
    }

    @Override
    public void plan() {
        System.out.println("客户要求需求变更计划...");
    }
}
