package com.iot.practice.designpattern.command;

/**
 * <p>Group 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月19日 15:11</p>
 * <p>@remark：项目组分成了三个组，每个组还是要接受增删改的命令</p>
 */
public abstract class Group {

    /**
     * 甲乙双方分开办公，你要和那个组讨论，你首先要找到这个组
     */
    public abstract void find();

    /**
     * 被要求增加功能
     */
    public abstract void add();

    /**
     * 被要求删除功能
     */
    public abstract void delete();

    /**
     * 被要求修改功能
     */
    public abstract void change();

    /**
     * 被要求给出所有的变更计划
     */
    public abstract void plan();
}
