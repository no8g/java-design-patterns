package com.iot.practice.designpattern.command;

/**
 * <p>NonCommandClient 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月19日 15:28</p>
 * <p>@remark：客户就是甲方，给我们钱的一方，是老大</p>
 */
public class NonCommandClient {

    public static void main(String[] args) {

        // 首先客户找到需求组说，过来谈需求，并修改
        System.out.println("-------------客户要求增加一个需求-----------------");

        Group rg = new RequirementGroup();

        //找到需求组
        rg.find();

        //增加一个需求
        rg.add();

        //要求变更计划
        rg.plan();

        //首先客户找到美工组说，过来谈页面，并修改
        System.out.println("-------------客户要求删除一个页面-----------------");
        Group pg = new PageGroup();
        //找到需求组
        pg.find();

        //增加一个需求
        pg.delete();

        //要求变更计划
        pg.plan();


    }
}
