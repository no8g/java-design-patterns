package com.iot.practice.designpattern.command;

/**
 * <p>CodeGroup 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月19日 15:27</p>
 * <p>@remark：代码组的职责是实现业务逻辑，当然包括数据库设计了</p>
 */
public class CodeGroup extends Group {

    @Override
    public void find() {
        System.out.println("找到代码组...");
    }

    @Override
    public void add() {
        System.out.println("客户要求增加一项功能...");
    }

    @Override
    public void delete() {
        System.out.println("客户要求删除一项功能...");
    }

    @Override
    public void change() {
        System.out.println("客户要求修改一项功能...");
    }

    @Override
    public void plan() {
        System.out.println("客户要求代码变更计划...");
    }
}
