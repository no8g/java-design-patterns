package com.iot.practice.designpattern.interpreter;

/**
 * <p>Expression 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月22日 16:24</p>
 * <p>@remark：创建一个表达式接口</p>
 */
public interface Expression {

    /**
     * 表达式接口
     *
     * @param context 文法
     * @return 是否为true
     */
    public boolean interpret(String context);
}
