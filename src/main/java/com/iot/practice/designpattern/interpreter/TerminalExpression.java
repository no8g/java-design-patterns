package com.iot.practice.designpattern.interpreter;

/**
 * <p>TerminalExpression 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月22日 16:26</p>
 * <p>@remark：创建实现了上述接口的实体类</p>
 */
public class TerminalExpression implements Expression {

    private String data;

    public TerminalExpression(String data) {
        this.data = data;
    }

    @Override
    public boolean interpret(String context) {

        return context.contains(this.data);
    }
}
