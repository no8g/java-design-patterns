package com.iot.practice.designpattern.interpreter;

/**
 * <p>OrExpression 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月22日 16:29</p>
 * <p>@remark：</p>
 */
public class OrExpression implements Expression {

    private Expression expression1 = null;

    private Expression expression2 = null;

    public OrExpression (Expression expression1, Expression expression2) {
        this.expression1 = expression1;
        this.expression2 = expression2;
    }

    @Override
    public boolean interpret(String context) {
        return this.expression1.interpret(context) || this.expression2.interpret(context);
    }
}
