package com.iot.practice.designpattern.interpreter;

/**
 * <p>InterpreterPatternClient 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月22日 16:33</p>
 * <p>@remark：InterpreterPatternClient 使用 Expression 类来创建规则，并解析它们。</p>
 */
public class InterpreterPatternClient {

    /**
     * 规则：Robert 和 John 是男性
     *
     * @return 表达式接口
     */
    public static Expression getMaleExpression() {
        Expression robert = new TerminalExpression("Robert");
        Expression john = new TerminalExpression("John");
        return new OrExpression(robert, john);
    }

    /**
     * 规则：Julie 是一个已婚的女性
     *
     * @return 表达式接口
     */
    public static Expression getMarriedWomanExpression() {
        Expression julie = new TerminalExpression("Julie");
        Expression married = new TerminalExpression("Married");
        return new AndExpression(julie, married);
    }

    public static void main(String[] args) {
        Expression isMale = getMaleExpression();
        Expression isMarriedWoman = getMarriedWomanExpression();

        System.out.println("Robert is male ? The answer is " + isMale.interpret("Robert"));
        System.out.println("Julie is a married woman ? The answer is " + isMarriedWoman.interpret("Julie Married"));
    }
}
