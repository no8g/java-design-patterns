package com.iot.practice.designpattern.chainofresponsibility;

/**
 * <p>Son 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月19日 10:23</p>
 * <p>@remark：</p>
 */
public class Son implements IHandler {

    /**
     * 目前向儿子请示
     *
     * @param iWomen 女性总称
     */
    @Override
    public void handleMessage(IWomen iWomen) {
        System.out.println("母亲的请示是：" + iWomen.getRequest());
        System.out.println("儿子的答复是：同意");
    }
}
