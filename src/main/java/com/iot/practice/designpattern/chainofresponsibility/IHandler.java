package com.iot.practice.designpattern.chainofresponsibility;

/**
 * <p>IHandler 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月19日 10:04</p>
 * <p>@remark：父系社会，那就是男性有至高权利，handler控制权</p>
 */
public interface IHandler {

    /**
     * 一个女性（女儿，妻子或者是母亲）要求逛街，你要处理这个请求
     *
     * @param iWomen
     */
    public void handleMessage(IWomen iWomen);
}
