package com.iot.practice.designpattern.chainofresponsibility;

/**
 * <p>WomenImpl 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月19日 9:56</p>
 * <p>@remark：女性就两个参数，一个是当前的个人状况，是结婚了呢还是没结婚，丈夫有没有去世，另外一个是要
 * 请示的内容，要出去逛街呀还是吃饭，我们看实现类：
 *
 * 我们使用数字来代表女性的不同状态，1 是未结婚，2 是已经结婚的，而且丈夫建在，3 是丈夫去世了的
 *
 * </p>
 */
public class WomenImpl implements IWomen {

    /**
     * 通过一个int类型的参数来描述妇女的个人状况
     * 1---未出嫁
     * 2---出嫁
     * 3---夫死
     */
    private int type = 0;

    /**
     * 妇女的请示
     */
    private String request = "";

    /**
     * 构造函数传递过来请求
     *
     * @param type    个人状况
     * @param request 妇女的请示
     */
    public WomenImpl(int type, String request) {
        this.type = type;
        this.request = request;
    }

    /**
     * 获得自己的状况
     *
     * @return 自己的状况
     */
    @Override
    public int getType() {
        return this.type;
    }

    /**
     * 获得妇女的请求
     *
     * @return 妇女的请求
     */
    @Override
    public String getRequest() {
        return this.request;
    }
}
