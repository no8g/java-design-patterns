package com.iot.practice.designpattern.chainofresponsibility;

/**
 * <p>IWomen 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月19日 9:55</p>
 * <p>@remark：古代悲哀女性的总称</p>
 */
public interface IWomen {

    /**
     * 获得个人状况
     *
     * @return 个人状况数值
     */
    public int getType();

    /**
     * 获得个人请示，你要干什么？出去逛街？约会?还是看电影
     *
     * @return 干什么
     */
    public String getRequest();
}
