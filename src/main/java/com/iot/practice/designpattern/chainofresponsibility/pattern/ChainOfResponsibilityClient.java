package com.iot.practice.designpattern.chainofresponsibility.pattern;

import com.iot.practice.designpattern.chainofresponsibility.IWomen;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * <p>Client 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月19日 11:37</p>
 * <p>@remark：</p>
 */
public class ChainOfResponsibilityClient {

    public static void main(String[] args) {
        // 随机挑选几个女性
        Random random = new Random();
        List<IWomen> iWomenList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            iWomenList.add(new IWomenImpl(random.nextInt(4), "我要看电影"));
        }

        // 定义三个请示对象
        Handler father = new Father();
        Handler husband = new Husband();
        Handler son = new Son();

        // 设置请示顺序
        father.setNext(husband);
        husband.setNext(son);

        for (IWomen iWomen : iWomenList) {
            father.HandleMessage(iWomen);
        }
    }
}
