package com.iot.practice.designpattern.chainofresponsibility.pattern;

import com.iot.practice.designpattern.chainofresponsibility.IWomen;

/**
 * <p>Husband 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月19日 11:15</p>
 * <p>@remark：</p>
 */
public class Husband extends Handler {

    /**
     * 丈夫只处理妻子的请求
     */
    public Husband() {
        super(2);
    }

    /**
     * 丈夫的答复
     *
     * @param iWomen 女性统称
     */
    @Override
    public void response(IWomen iWomen) {
        System.out.println("--------妻子向丈夫请示-------");
        System.out.println(iWomen.getRequest());
        System.out.println("丈夫的答复是：同意\n");
    }
}
