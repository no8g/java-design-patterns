package com.iot.practice.designpattern.chainofresponsibility.pattern;

import com.iot.practice.designpattern.chainofresponsibility.IWomen;

/**
 * <p>Father 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月19日 11:13</p>
 * <p>@remark：</p>
 */
public class Father extends Handler {

    /**
     * 父亲只处理女儿的请求
     */
    public Father() {
        super(1);
    }

    /**
     * 父亲的答复
     *
     * @param iWomen 女性统称
     */
    @Override
    public void response(IWomen iWomen) {
        System.out.println("--------女儿向父亲请示-------");
        System.out.println(iWomen.getRequest());
        System.out.println("父亲的答复是:同意\n");
    }
}
