package com.iot.practice.designpattern.chainofresponsibility;

/**
 * <p>Husband 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月19日 10:14</p>
 * <p>@remark：</p>
 */
public class Husband implements IHandler {

    /**
     * 目前向丈夫请示
     *
     * @param iWomen 女性总称
     */
    @Override
    public void handleMessage(IWomen iWomen) {
        System.out.println("妻子的请示是：" + iWomen.getRequest());
        System.out.println("丈夫的答复是：同意");
    }
}
