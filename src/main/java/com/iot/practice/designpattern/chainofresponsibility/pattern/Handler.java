package com.iot.practice.designpattern.chainofresponsibility.pattern;

import com.iot.practice.designpattern.chainofresponsibility.IWomen;

/**
 * <p>Handler 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月19日 10:56</p>
 * <p>@remark：
 * 有没有看到，其实在这里也用到模版方法模式，在模版方法中判断请求的级别和当前能够处理的级别，
 * 如果相同则调用基本方法，做出反馈；如果不相等，则传递到下一个环节，由下一环节做出回应。基本方
 * 法 response 要各个实现类都要实现，我们来看三个实现类：
 * </p>
 */
public abstract class Handler {

    /**
     * 能处理的级别
     */
    private int level = 0;

    /**
     * 责任传递，下一个人责任人是谁
     */
    private Handler nextHandler;

    /**
     * 构造函数：每个类都要说明一下自己能处理哪些请求
     *
     * @param level 能处理的级别
     */
    public Handler(int level) {
        this.level = level;
    }

    /**
     * 一个女性（女儿，妻子或者是母亲）要求逛街，你要处理这个请求
     *
     * @param iWomen 女性统称
     */
    public final void HandleMessage(IWomen iWomen) {
        if (iWomen.getType() == this.level) {
            this.response(iWomen);
        } else {
            // 有后续环节，才把请求往后递送
            if (this.nextHandler != null) {
                this.nextHandler.HandleMessage(iWomen);
            } else {
                // /已经没有后续处理人了，不用处理了
                System.out.println("-----------没地方请示了，不做处理！---------\n");
            }
        }
    }

    /**
     * 如果不属于你处理的返回，你应该让她找下一个环节的人，比如
     * 女儿出嫁了，还向父亲请示是否可以逛街，那父亲就应该告诉女儿，应该找丈夫请示
     *
     * @param handler 处理类
     */
    public void setNext(Handler handler) {
        this.nextHandler = handler;
    }

    /**
     * 有请示那当然要回应
     *
     * @param iWomen 女性统称
     */
    public abstract void response(IWomen iWomen);
}
