package com.iot.practice.designpattern.chainofresponsibility.pattern;

import com.iot.practice.designpattern.chainofresponsibility.IWomen;

/**
 * <p>IWomenImpl 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月19日 11:23</p>
 * <p>@remark：</p>
 */
public class IWomenImpl implements IWomen {

    /**
     * 通过一个int类型的参数来描述妇女的个人状况
     * 1---未出嫁
     * 2---出嫁
     * 3---夫死
     */
    private int type = 0;

    /**
     * 妇女的请示
     */
    private String request = "";

    /**
     * 构造函数传递过来请求
     *
     * @param type    个人状况
     * @param request 妇女的请示
     */
    public IWomenImpl(int type, String request) {
        this.type = type;
        this.request = request;
        // 为了显示好看点，我在这里做了点处理
        switch(this.type) {
            case 1:
                this.request = "女儿的请求是：" + request;
                break;
            case 2:
                this.request = "妻子的请求是：" + request;
                break;
            case 3:
                this.request = "母亲的请求是：" + request;
                break;
        }
    }

    /**
     * 获得自己的状况
     *
     * @return 自己的状况
     */
    @Override
    public int getType() {
        return this.type;
    }

    /**
     * 获得妇女的请求
     *
     * @return 妇女的请求
     */
    @Override
    public String getRequest() {
        return this.request;
    }
}
