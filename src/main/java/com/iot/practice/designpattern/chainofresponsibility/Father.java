package com.iot.practice.designpattern.chainofresponsibility;

/**
 * <p>Father 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月19日 10:08</p>
 * <p>@remark：父亲、丈夫、儿子都是这个 IHandler 接口的实现者：</p>
 */
public class Father implements IHandler {

    /**
     * 未出嫁女儿来请示父亲
     *
     * @param iWomen 女性总称
     */
    @Override
    public void handleMessage(IWomen iWomen) {
        System.out.println("女儿的请示是：" + iWomen.getRequest());
        System.out.println("父亲的答复是：同意");
    }
}
