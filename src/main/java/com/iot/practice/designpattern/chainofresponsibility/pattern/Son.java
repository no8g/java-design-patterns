package com.iot.practice.designpattern.chainofresponsibility.pattern;

import com.iot.practice.designpattern.chainofresponsibility.IWomen;

/**
 * <p>Son 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月19日 11:18</p>
 * <p>@remark：</p>
 */
public class Son extends Handler {

    /**
     * 儿子只处理母亲的请求
     */
    public Son() {
        super(3);
    }

    /**
     * 儿子的答复
     *
     * @param iWomen 女性统称
     */
    @Override
    public void response(IWomen iWomen) {
        System.out.println("--------母亲向儿子请示-------");
        System.out.println(iWomen.getRequest());
        System.out.println("儿子的答复是：同意\n");
    }
}
