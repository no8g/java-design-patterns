package com.iot.practice.designpattern.chainofresponsibility;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * <p>NonDesignPatternClient 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月19日 10:25</p>
 * <p>@remark：我们后人来看这样的社会道德</p>
 */
public class NonDesignPatternClient {

    public static void main(String[] args) {

        // 随机挑选几个女性
        Random random = new Random();
        List<IWomen> iWomenList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            iWomenList.add(new WomenImpl(random.nextInt(4), "我要出去逛街"));
        }

        // 定义三个请示对象
        IHandler father = new Father();
        IHandler husband = new Husband();
        IHandler son = new Son();

        for (IWomen iWomen : iWomenList) {
            // 未结婚少女，请示父亲
            if (iWomen.getType() == 1) {
                System.out.println("\n--------女儿向父亲请示-------");
                father.handleMessage(iWomen);
            } else if (iWomen.getType() == 2) {
                System.out.println("\n--------妻子向丈夫请示-------");
                husband.handleMessage(iWomen);
            } else if (iWomen.getType() == 3) {
                System.out.println("\n--------母亲向儿子请示-------");
                son.handleMessage(iWomen);
            } else {
                // 暂时啥也不做
                System.out.println("nothing");
            }
        }
    }
}
