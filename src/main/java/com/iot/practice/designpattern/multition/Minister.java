package com.iot.practice.designpattern.multition;

/**
 * <p>Minister 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年01月25日 17:34</p>
 * <p>@remark：
 * 大臣们悲惨了，一个皇帝都伺候不过来了，现在还来了两个个皇帝
 * TND，不管了，找到个皇帝，磕头，请按就成了！
 * </p>
 */
public class Minister {

    public static void main(String[] args) {

        // 10个大臣
        int ministerNum = 10;

        for (int i = 0; i < ministerNum; i++) {
            Emperor emperor = Emperor.getInstance();
            System.out.print("第" + (i + 1) + "个大臣参拜的是： ");
            emperor.emperorInfo();
        }
    }
}
