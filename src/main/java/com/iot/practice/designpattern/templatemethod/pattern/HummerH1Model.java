package com.iot.practice.designpattern.templatemethod.pattern;

/**
 * <p>HummerH1Model 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月29日 16:26</p>
 * <p>@remark：</p>
 */
public class HummerH1Model extends HummerModel {

    @Override
    public void start() {
        System.out.println("悍马H1发动...");
    }

    @Override
    public void stop() {
        System.out.println("悍马H1停车...");
    }

    @Override
    public void alarm() {
        System.out.println("悍马H1鸣笛...");
    }

    @Override
    public void engineBoom() {
        System.out.println("悍马H1引擎声音是这样在...");
    }
}
