package com.iot.practice.designpattern.templatemethod.pattern;

/**
 * <p>TemplateMethodClient 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月29日 16:42</p>
 * <p>@remark：</p>
 */
public class TemplateMethodClient {

    public static void main(String[] args) {

        // 客户开着H1型号，出去遛弯了
        HummerH1Model h1Model = new HummerH1Model();
        h1Model.run();

        System.out.println("\n");

        // 客户开H2型号，出去玩耍了
        HummerH2Model h2Model = new HummerH2Model();
        h2Model.run();
    }
}
