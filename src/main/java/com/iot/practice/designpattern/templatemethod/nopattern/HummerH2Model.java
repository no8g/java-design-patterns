package com.iot.practice.designpattern.templatemethod.nopattern;

/**
 * <p>HummerH2Model 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月29日 16:07</p>
 * <p>@remark：</p>
 */
public class HummerH2Model extends HummerModel {

    @Override
    public void start() {
        System.out.println("悍马H2发动...");
    }

    @Override
    public void stop() {
        System.out.println("悍马H2停车...");
    }

    @Override
    public void alarm() {
        System.out.println("悍马H2鸣笛...");
    }

    @Override
    public void engineBoom() {
        System.out.println("悍马H2引擎声音是那样在...");
    }

    @Override
    public void run() {
        // 先发动汽车
        this.start();
        // 引擎开始轰鸣
        this.engineBoom();
        // 然后就开始跑了，跑的过程中遇到一条狗挡路，就按喇叭
        this.alarm();
        // 到达目的地就停车
        this.stop();
    }

    /**
     * 然后程序写到这里，你就看到问题了，run 方法的实现应该在抽象类上，不应该在实现类上
     */
}
