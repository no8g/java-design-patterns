package com.iot.practice.designpattern.iterator.pattern;

/**
 * <p>IProject 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月23日 15:15</p>
 * <p>@remark：</p>
 */
public interface IProject {

    /**
     * 增加项目
     *
     * @param name 项目名称
     * @param num 项目成员数量
     * @param cost 项目费用
     */
    public void add(String name,int num,int cost);

    /**
     * 从老板这里看到的就是项目信息
     *
     * @return 项目信息
     */
    public String getProjectInfo();

    /**
     * 获得一个可以被遍历的对象
     *
     * @return 一个可以被遍历的对象
     */
    public IProjectIterator iterator();
}
