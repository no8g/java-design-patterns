package com.iot.practice.designpattern.iterator.pattern;

import java.util.ArrayList;

/**
 * <p>ProjectIterator 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月23日 15:27</p>
 * <p>@remark：定义一个迭代器</p>
 */
public class ProjectIterator implements IProjectIterator {

    /**
     * 所有的项目都放在这里ArrayList中
     */
    private ArrayList<IProject> projectList = new ArrayList<IProject>();

    private int currentItem = 0;

    /**
     * 构造函数传入projectList
     *
     * @param projectList 所有项目集合
     */
    public ProjectIterator(ArrayList<IProject> projectList) {
        this.projectList = projectList;
    }

    /**
     * 判断是否还有元素，必须实现
     *
     * @return 是否为true
     */
    @Override
    public boolean hasNext() {
        // 定义一个返回值
        boolean b = true;
        if (this.currentItem >= projectList.size() || this.projectList.get(this.currentItem) == null) {
            b = false;
        }
        return b;
    }

    /**
     * 取得下一个值，必须实现
     *
     * @return 下一个值
     */
    @Override
    public IProject next() {
        return this.projectList.get(this.currentItem++);
    }
}
