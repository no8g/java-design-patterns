package com.iot.practice.designpattern.iterator.pattern;

import java.util.Iterator;

/**
 * <p>IProjectIterator 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月23日 15:26</p>
 * <p>@remark：
 * 大家可能很奇怪，你定义的这个接口方法、变量都没有，有什么意义呢？有意义，所有的 Java 书上都一直
 * 说是面向接口编程，你的接口是对一个事物的描述，也就是说我通过接口就知道这个事物有哪些方法，哪些属性，
 * 我们这里的 IProjectIterator 是要建立一个指向 Project 类的迭代器，目前暂时定义的就是一个通用的迭
 * 代器，可能以后会增加 IProjectIterator 的一些属性或者方法。当然了，你也可以在实现类上实现两个接口，
 * 一个是 Iterator,一个是 IProjectIterator（这时候，这个接口就不用继承 Iterator），杀猪杀尾巴，
 * 各有各的杀发。
 * </p>
 */
public interface IProjectIterator extends Iterator {
}
