package com.iot.practice.designpattern.iterator;

/**
 * <p>IProject 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月23日 14:47</p>
 * <p>@remark：</p>
 */
public interface IProject {

    /**
     * 从老板这里看到的就是项目信息
     *
     * @return 项目信息
     */
    public String getProjectInfo();
}
