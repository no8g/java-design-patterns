package com.iot.practice.designpattern.abstractfactory.whitehuman;

/**
 * <p>WhiteFemaleHuman 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月02日 11:47</p>
 * <p>@remark：</p>
 */
public class WhiteFemaleHuman extends AbstractWhiteHuman {

    @Override
    public void sex() {
        System.out.println("该白种人的性别为女...");
    }
}
