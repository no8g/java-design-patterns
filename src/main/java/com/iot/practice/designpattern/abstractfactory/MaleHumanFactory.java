package com.iot.practice.designpattern.abstractfactory;

/**
 * <p>MaleHumanFactory 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月04日 10:37</p>
 * <p>@remark：男性创建工厂</p>
 */
public class MaleHumanFactory extends AbstractHumanFactory {

    /**
     * 创建一个男性白种人
     *
     * @return 白种人
     */
    @Override
    public Human createWhiteHuman() {
        return super.createHuman(HumanEnum.WhiteMaleHuman);
    }

    /**
     * 创建一个男性黑种人
     *
     * @return 黑种人
     */
    @Override
    public Human createBlackHuman() {
        return super.createHuman(HumanEnum.BlackMaleHuman);
    }

    /**
     * 创建一个男性黄种人
     *
     * @return 黄种人
     */
    @Override
    public Human createYellowHuman() {
        return super.createHuman(HumanEnum.YellowMaleHuman);
    }
}
