package com.iot.practice.designpattern.abstractfactory.blackhuman;

/**
 * <p>BlackFemaleHuman 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月02日 14:13</p>
 * <p>@remark：</p>
 */
public class BlackFemaleHuman extends AbstractBlackHuman {

    @Override
    public void sex() {
        System.out.println("该黑种人的性别为女...");
    }
}
