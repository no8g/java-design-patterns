package com.iot.practice.designpattern.abstractfactory.yellowhuman;

/**
 * <p>YellowMaleHuman 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月02日 16:32</p>
 * <p>@remark：</p>
 */
public class YellowMaleHuman extends AbstractYellowHuman {
    @Override
    public void sex() {
        System.out.println("该黄种人的性别为男...");
    }
}
