package com.iot.practice.designpattern.abstractfactory.yellowhuman;

import com.iot.practice.designpattern.abstractfactory.Human;

/**
 * <p>AbstractYellowHuman 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月02日 10:51</p>
 * <p>@remark：为什么要修改成抽象类呢？要定义性别呀</p>
 */
public abstract class AbstractYellowHuman implements Human {

    @Override
    public void laugh() {
        System.out.println("黄色人种会大笑，幸福呀！ ");
    }

    @Override
    public void cry() {
        System.out.println("黄色人种会哭");
    }

    @Override
    public void talk() {
        System.out.println("黄色人种会说话，一般说的都是双字节");
    }
}
