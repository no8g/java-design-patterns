package com.iot.practice.designpattern.abstractfactory;

/**
 * <p>AbstractHumanFactory 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月04日 10:30</p>
 * <p>@remark：编写一个抽象类，根据enum创建一个人类出来</p>
 */
public abstract class AbstractHumanFactory implements HumanFactory {

    /**
     * 编写一个抽象类，根据enum创建一个人类出来
     *
     * @param humanEnum HumanEnum 类型
     * @return 人类
     */
    protected Human createHuman(HumanEnum humanEnum) {

        Human human = null;

        // 如果传递进来不是一个Enum中具体的一个Element的话，则不处理
        if (!"".equals(humanEnum.getValue())){
            try {
                // 直接产生一个实例
                human = (Human) Class.forName(humanEnum.getValue()).newInstance();
            } catch (Exception e) {
                // 因为使用了enum，这个种异常情况不会产生了，除非你的enum有问题；
                e.printStackTrace();
            }
        }

        return human;
    }
}
