package com.iot.practice.designpattern.abstractfactory;

/**
 * <p>NvWa 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年01月26日 18:16</p>
 * <p>@remark：
 * 女娲建立起了两条生产线，分别是：
 * 男性生产线
 * 女性生产线
 * </p>
 */
public class NvWa2 {

    public static void main(String[] args) {

        // 第一条生产线，男性生产线
        MaleHumanFactory maleHumanFactory = new MaleHumanFactory();

        // 第一条生产线，男性生产线
        FemaleHumanFactory femaleHumanFactory = new FemaleHumanFactory();

        // 生产线建立完毕，开始生产人了:
        Human maleYellowHuman = maleHumanFactory.createYellowHuman();
        Human femaleYellowHuman = femaleHumanFactory.createYellowHuman();

        maleYellowHuman.cry();
        maleYellowHuman.laugh();
        maleYellowHuman.cry();
        maleYellowHuman.sex();

        femaleYellowHuman.cry();
        femaleYellowHuman.laugh();
        femaleYellowHuman.cry();
        femaleYellowHuman.sex();

        // 后面你可以续了......
    }
}
