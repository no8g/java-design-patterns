package com.iot.practice.designpattern.abstractfactory;

/**
 * <p>HumanFactory 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月02日 16:46</p>
 * <p>@remark：
 * 这次定一个接口，应该要造不同性别的人，需要不同的生产线
 * 那这个八卦炉必须可以制造男人和女人
 * </p>
 */
public interface HumanFactory {

    /**
     * 制造白色人种
     *
     * @return 白色人种
     */
    public Human createWhiteHuman();

    /**
     * 制造黑色人种
     *
     * @return 黑色人种
     */
    public Human createBlackHuman();

    /**
     * 制造黄色人种
     *
     * @return 黄色人种
     */
    public Human createYellowHuman();
}
