package com.iot.practice.designpattern.abstractfactory.whitehuman;

import com.iot.practice.designpattern.abstractfactory.Human;

/**
 * <p>AbstractWhiteHuman 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月02日 10:54</p>
 * <p>@remark：白色人人种：为了代码整洁，新建一个包，这里是白种人的天下了</p>
 */
public abstract class AbstractWhiteHuman implements Human {

    @Override
    public void laugh() {
        System.out.println("白色人种会大笑，侵略的笑声");
    }

    @Override
    public void cry() {
        System.out.println("白色人种会哭");
    }

    @Override
    public void talk() {
        System.out.println("白色人种会说话，一般都是但是单字节！");
    }
}
