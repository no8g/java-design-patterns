package com.iot.practice.designpattern.abstractfactory;

/**
 * <p>FemaleHumanFactory 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月04日 14:48</p>
 * <p>@remark：女性创建工厂</p>
 */
public class FemaleHumanFactory extends AbstractHumanFactory {

    /**
     * 创建一个女性白种人
     *
     * @return 白种人
     */
    @Override
    public Human createWhiteHuman() {
        return super.createHuman(HumanEnum.WhiteFemaleHuman);
    }

    /**
     * 创建一个女性黑种人
     *
     * @return 黑种人
     */
    @Override
    public Human createBlackHuman() {
        return super.createHuman(HumanEnum.BlackFemaleHuman);
    }

    /**
     * 创建一个女性黄种人
     *
     * @return 黄种人
     */
    @Override
    public Human createYellowHuman() {
        return super.createHuman(HumanEnum.YellowFemaleHuman);
    }
}
