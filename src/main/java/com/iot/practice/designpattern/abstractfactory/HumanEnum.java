package com.iot.practice.designpattern.abstractfactory;

/**
 * <p>HumanEnum 此枚举用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月02日 16:36</p>
 * <p>@remark：世界上有哪些类型的人，列出来</p>
 */
public enum HumanEnum {

    // 把世界上所有人类型都定义出来
    YellowFemaleHuman("com.uiot.practice.designpattern.abstractfactory.yellowhuman.YellowFemaleHuman"),
    YellowMaleHuman("com.uiot.practice.designpattern.abstractfactory.yellowhuman.YellowMaleHuman"),
    WhiteFemaleHuman("com.uiot.practice.designpattern.abstractfactory.whitehuman.WhiteFemaleHuman"),
    WhiteMaleHuman("com.uiot.practice.designpattern.abstractfactory.whitehuman.WhiteMaleHuman"),
    BlackFemaleHuman("com.uiot.practice.designpattern.abstractfactory.blackhuman.BlackFemaleHuman"),
    BlackMaleHuman("com.uiot.practice.designpattern.abstractfactory.blackhuman.BlackMaleHuman");

    private String value = "";

    /**
     * 定义构造函数，目的是Data(value)类型的相匹配
     *
     * @param value 值
     */
    private HumanEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
