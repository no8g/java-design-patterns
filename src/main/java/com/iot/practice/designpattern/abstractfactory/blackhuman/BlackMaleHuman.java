package com.iot.practice.designpattern.abstractfactory.blackhuman;

/**
 * <p>BlackMaleHuman 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月02日 16:31</p>
 * <p>@remark：</p>
 */
public class BlackMaleHuman extends AbstractBlackHuman{

    @Override
    public void sex() {
        System.out.println("该黑种人的性别为男...");
    }
}
