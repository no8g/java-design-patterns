package com.iot.practice.designpattern.observer.pattern;

/**
 * <p>ConcreteNbaObserver 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月03日 14:25</p>
 * <p>@remark：ConcreteStockObserver是看股票的同事，ConcreteNbaObserver是看NBA的同事，作为具体观察者，继承Observer类。这里只给出ConcreteStockObserver类的代码，ConcreteNbaObserver类与之类似。</p>
 */
public class ConcreteNbaObserver extends AbstractObserver {

    public ConcreteNbaObserver(String name, SubjectInformer subjectInformer) {
        super(name, subjectInformer);
    }

    @Override
    public void update() {
        System.out.println(super.subjectInformer.getAction() + "\n" + name + "关闭NBA球赛，继续工作");
    }
}
