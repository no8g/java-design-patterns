package com.iot.practice.designpattern.observer.pattern;

/**
 * <p>ClientBoss 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月03日 14:40</p>
 * <p>@remark：
 *  老板作为通知者进行通知（ClientBoss）
 *  老板作为通知者，通知观察者。这里将tom从老板的通知列表中移除，老板只通知到了jack。
 * </p>
 */
public class ClientBoss {

    public static void main(String[] args) {
        // 老板作为通知者
        ConcreteInformerSecretary secretary = new ConcreteInformerSecretary();

        ConcreteStockObserver jack = new ConcreteStockObserver("jack", secretary);
        ConcreteNbaObserver tom = new ConcreteNbaObserver("tom", secretary);

        // 老板通知
        secretary.attach(jack);
        secretary.attach(tom);

        // tom没被老板通知到，所以不用挨骂
        secretary.detach(tom);

        // 老板回来了
        secretary.setAction("咳咳，我大Boss回来了！");

        // 发通知
        secretary.notifyObservers();
    }
}
