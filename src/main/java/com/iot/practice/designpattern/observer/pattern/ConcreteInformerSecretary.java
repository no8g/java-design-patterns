package com.iot.practice.designpattern.observer.pattern;

import java.util.LinkedList;
import java.util.List;

/**
 * <p>ConcreteInformerSecretary 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月03日 14:01</p>
 * <p>@remark：前台Secretary和老板Boss作为具体通知者，实现Subject接口。这里只给出Secretary类的代码，Boss类与之类似。</p>
 */
public class ConcreteInformerSecretary implements SubjectInformer {

    /**
     * 同事列表
     */
    private List<AbstractObserver> observers = new LinkedList<>();
    private String action;

    /**
     * 添加
     *
     * @param abstractObserver 观察者
     */
    @Override
    public void attach(AbstractObserver abstractObserver) {
        observers.add(abstractObserver);
    }

    /**
     * 删除
     *
     * @param abstractObserver 观察者
     */
    @Override
    public void detach(AbstractObserver abstractObserver) {
        observers.remove(abstractObserver);
    }

    /**
     * 通知
     */
    @Override
    public void notifyObservers() {
        for (AbstractObserver observer : observers) {
            observer.update();
        }
    }

    @Override
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * 前台状态
     *
     * @return 前台状态
     */
    @Override
    public String getAction() {
        return this.action;
    }
}
