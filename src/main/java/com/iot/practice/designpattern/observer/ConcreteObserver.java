package com.iot.practice.designpattern.observer;

/**
 * <p>ConcreteObserver 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月25日 14:59</p>
 * <p>@remark：具体观察者，实现Observer接口。</p>
 */
public class ConcreteObserver implements Observer {

    @Override
    public void update() {
        System.out.println("收到通知，正在进行处理...");
    }
}
