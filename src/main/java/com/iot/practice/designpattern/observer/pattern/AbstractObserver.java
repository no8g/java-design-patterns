package com.iot.practice.designpattern.observer.pattern;

/**
 * <p>AbstractObserver 此类用于：观察者</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月03日 11:45</p>
 * <p>@remark：下面举一个具体实例，假设上班时间有一部分同事在看股票，一部分同事在看NBA，这时老板回来了，
 * 前台通知了部分同事老板回来了，这些同事及时关闭了网页没被发现，而没被通知到的同事被抓了个现行，被老板亲自“通知”关闭网页，UML图如下：</p>
 */
public abstract class AbstractObserver {

    protected String name;

    protected SubjectInformer subjectInformer;

    public AbstractObserver(String name, SubjectInformer subjectInformer) {
        this.name = name;
        this.subjectInformer = subjectInformer;
    }

    /**
     * 更新的抽象方法
     */
    public abstract void update();
}
