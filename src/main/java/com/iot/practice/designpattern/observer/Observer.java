package com.iot.practice.designpattern.observer;

/**
 * <p>Observer 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月25日 14:47</p>
 * <p>@remark：观察者一般是一个接口，每一个实现该接口的实现类都是具体观察者。</p>
 */
public interface Observer {

    /**
     * 更新
     */
    public void update();
}
