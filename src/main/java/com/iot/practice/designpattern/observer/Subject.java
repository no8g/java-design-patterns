package com.iot.practice.designpattern.observer;

import java.util.Vector;

/**
 * <p>Subject 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月25日 14:45</p>
 * <p>@remark：主题Subject</p>
 */
public class Subject {

    /**
     * 观察者数组集合
     */
    private Vector<Observer> vector = new Vector<>();

    /**
     * 增加一个观察者
     *
     * @param observer 观察者
     */
    public void addObserver(Observer observer) {
        this.vector.add(observer);
    }

    /**
     * 删除一个观察者
     *
     * @param observer 观察者
     */
    public void deleteObserver(Observer observer) {
        this.vector.remove(observer);
    }

    /**
     * 通知所有观察者
     */
    public void notifyObserver() {
        for (Observer observer : this.vector) {
            observer.update();
        }
    }
}
