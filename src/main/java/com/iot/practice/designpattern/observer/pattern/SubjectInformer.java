package com.iot.practice.designpattern.observer.pattern;

/**
 * <p>SubjectInformer 此接口用于：通知者接口</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月03日 11:44</p>
 * <p>@remark：
 * 下面举一个具体实例，假设上班时间有一部分同事在看股票，一部分同事在看NBA，这时老板回来了，前台通知了部分同事老板回来了，
 * 这些同事及时关闭了网页没被发现，而没被通知到的同事被抓了个现行，被老板亲自“通知”关闭网页，UML图如下： *
 * </p>
 */
public interface SubjectInformer {

    /**
     * 增加
     *
     * @param abstractObserver 观察者
     */
    public void attach(AbstractObserver abstractObserver);

    /**
     * 删除
     *
     * @param abstractObserver 观察者
     */
    public void detach(AbstractObserver abstractObserver);

    /**
     * 通知
     */
    public void notifyObservers();

    /**
     * 状态，set方法
     *
     * @param action 动作
     */
    public void setAction(String action);
    /**
     * 状态，get方法
     */
    public String getAction();
}

