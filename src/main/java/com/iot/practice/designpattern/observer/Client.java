package com.iot.practice.designpattern.observer;

/**
 * <p>Client 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月25日 15:01</p>
 * <p>@remark：Client客户端，首先创建一个被观察者，然后定义一个观察者，将该被观察者添加到该观察者的观察者数组中，进行测试。</p>
 */
public class Client {

    public static void main(String[] args) {

        // 创建一个主题
        ConcreteSubject subject = new ConcreteSubject();

        // 定义一个观察者
        Observer observer = new ConcreteObserver();

        // 将一个观察者添加到主题中
        subject.addObserver(observer);

        // 通知
        subject.doSomething();
    }
}
