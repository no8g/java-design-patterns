package com.iot.practice.designpattern.observer;

/**
 * <p>ConcreteSubject 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月25日 14:56</p>
 * <p>@remark：继承Subject类，在这里实现具体业务，在具体项目中，该类会有很多变种</p>
 */
public class ConcreteSubject extends Subject {

    /**
     * 具体业务逻辑
     */
    public void doSomething() {
        super.notifyObserver();
    }
}
