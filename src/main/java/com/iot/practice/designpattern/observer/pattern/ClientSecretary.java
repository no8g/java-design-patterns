package com.iot.practice.designpattern.observer.pattern;

/**
 * <p>ClientSecretary 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月03日 14:29</p>
 * <p>@remark：
 * 前台作为通知者进行通知（ClientSecretary）
 * 前台作为通知者，通知观察者。这里添加jack和tom到通知列表，并从通知列表中删除了jack，测试没在通知列表中的对象不会收到通知。
 * </p>
 */
public class ClientSecretary {

    public static void main(String[] args) {

        // 前台为通知者
        ConcreteInformerSecretary secretary = new ConcreteInformerSecretary();
        ConcreteStockObserver jack = new ConcreteStockObserver("jack", secretary);
        ConcreteNbaObserver tom = new ConcreteNbaObserver("tom", secretary);

        // 前台通知
        secretary.attach(jack);
        secretary.attach(tom);

        // jack没被前台通知到，所以被老板抓了个现行
        secretary.detach(jack);

        // 老板回来了
        secretary.setAction("小心！Boss回来了！");
        // 发通知
        secretary.notifyObservers();
    }
}
