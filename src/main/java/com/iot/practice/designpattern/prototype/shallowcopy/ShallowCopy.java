package com.iot.practice.designpattern.prototype.shallowcopy;

import java.util.ArrayList;

/**
 * <p>ShallowCopy 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月06日 14:52</p>
 * <p>@remark：</p>
 */
public class ShallowCopy implements Cloneable {

    /**
     * 定义一个私有变量
     */
    private ArrayList<String> arrayList = new ArrayList<>();

    @Override
    public ShallowCopy clone() {

        ShallowCopy shallowCopy = null;

        try {
            shallowCopy = (ShallowCopy) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        return shallowCopy;
    }

    /**
     * 取得arrayList的值
     */
    public ArrayList<String> getValue() {
        return this.arrayList;
    }

    /**
     * 设置List的值
     *
     * @param value 值
     */
    public void setValue(String value) {
        this.arrayList.add(value);
    }
}
