package com.iot.practice.designpattern.prototype;

/**
 * <p>AdvTemplate 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月06日 10:01</p>
 * <p>@remark：在类图中 AdvTemplate 是广告信的模板，一般都是从数据库取出，生成一个 BO 或者是 DTO，我们这里
 * 使用一个静态的值来做代表；Mail 类是一封邮件类，发送机发送的就是这个类，我们先来看看我们的程序：</p>
 */
public class AdvTemplate {

    /**
     * 广告信名称
     */
    private String advSubject = "XX银行国庆信用卡抽奖活动";

    /**
     * 广告信内容
     */
    private String advContent = "国庆抽奖活动通知：只要刷卡就送你1百万！....";

    /**
     * 取得广告信的名称
     *
     * @return 广告信的名称
     */
    public String getAdvSubject() {
        return this.advSubject;
    }

    /**
     * 取得广告信的内容
     *
     * @return 广告信的内容
     */
    public String getAdvContent() {
        return this.advContent;
    }
}
