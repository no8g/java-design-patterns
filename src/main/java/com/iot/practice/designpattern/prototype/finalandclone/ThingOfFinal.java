package com.iot.practice.designpattern.prototype.finalandclone;

import java.util.ArrayList;

/**
 * <p>ThingOfFinal 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月06日 14:37</p>
 * <p>@remark：Clone 与 final 两对冤家。对象的 clone 与对象内的 final 属性是由冲突的，我们举例来说明这个问题:</p>
 */
public class ThingOfFinal implements Cloneable {

    /**
     * 定义一个私有变量
     */
    private final ArrayList<String> arrayList = new ArrayList<>();

    @Override
    public ThingOfFinal clone() {
        ThingOfFinal thingOfFinal = null;

        try {
            thingOfFinal = (ThingOfFinal) super.clone();
            // 以下语句编辑器会报错的
            // this.arrayList = (ArrayList<String>) this.arrayList.clone();
            // final 类型你还想重写设值呀！完蛋了，你要实现深拷贝的梦想在 final 关键字的威胁下破灭了。
            // 其实路还是有的，删除掉 final 关键字，这是最便捷最安全最快速的方式，你要使用 clone 方法就在类的成员变量上不要增加 final 关键字。
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        return thingOfFinal;
    }
}
