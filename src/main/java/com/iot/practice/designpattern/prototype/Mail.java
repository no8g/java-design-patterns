package com.iot.practice.designpattern.prototype;

import lombok.Data;

/**
 * <p>Mail 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月06日 10:05</p>
 * <p>@remark：Mail 就是一个业务对象，我们再来看业务场景类是怎么调用的：</p>
 */
@Data
public class Mail implements Cloneable {

    /**
     * 收件人
     */
    private String receiver;

    /**
     * 邮件名称
     */
    private String subject;

    /**
     * 称谓
     */
    private String appellation;

    /**
     * 邮件内容
     */
    private String context;

    /**
     * 邮件的尾部，一般都是加上“XXX版权所有”等信息
     */
    private String tail;

    /**
     * 构造函数
     */
    public Mail(AdvTemplate advTemplate) {
        this.context = advTemplate.getAdvContent();
        this.subject = advTemplate.getAdvSubject();
    }

    /**
     * 增加了一个 Cloneable 接口， Mail 实现了这个接口，在 Mail 类中重写了 clone()方法，我们来看 Mail类的改变：
     *
     * 在 clone()方法上增加了一个注解@Override，没有继承一个类为什么可以重写呢？在 Java 中所有类的
     * 老祖宗是谁？对嘛，Object 类，每个类默认都是继承了这个类，所以这个用上重写是非常正确的。
     *
     * @return Mail
     */
    @Override
    public Mail clone() {

        Mail mail = null;
        try {
            mail = (Mail) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        return mail;
    }
}
