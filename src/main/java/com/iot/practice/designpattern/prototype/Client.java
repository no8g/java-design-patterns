package com.iot.practice.designpattern.prototype;

import java.util.Random;

/**
 * <p>Client 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月06日 10:07</p>
 * <p>@remark：</p>
 */
public class Client {

    /**
     * 发送账单的数量，这个值是从数据库中获得
     */
    public static final int MAX_COUNT = 10000;

    public static void main(String[] args) {
        // 模拟发送邮件
        int i = 0;

        // 把模板定义出来，这个是从数据库中获得
        Mail mail = new Mail(new AdvTemplate());
        mail.setTail("XX银行版权所有");
        long startTime = System.currentTimeMillis();
        while (i < MAX_COUNT) {
            // 以下是每封邮件不同的地方
            Mail cloneMail = mail.clone();
            cloneMail.setAppellation(getRandString(5) + "先生（女士）");
            cloneMail.setReceiver(getRandString(8) + "@" + getRandString(5) + ".com");

            // 然后发送邮件
            sendMail(cloneMail);
            i++;
        }

        System.out.println("总共耗时： " + (System.currentTimeMillis() - startTime) + "毫秒");
    }

    public static void sendMail(Mail mail) {
        System.out.println("标题：" + mail.getSubject() + "\t收件人：" + mail.getReceiver() + "\t .....发送成功！");
    }

    /**
     * 获得指定长度的随机字符串
     *
     * @return 随机字符串
     */
    public static String getRandString(int maxLength) {

        String source ="abcdefghijklmnopqrskuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < maxLength; i++) {
            sb.append(source.charAt(random.nextInt(source.length())));
        }

        return sb.toString();
    }
}
