package com.iot.practice.designpattern.prototype.shallowcopy;

/**
 * <p>ShallowCopyClient 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月06日 14:57</p>
 * <p>@remark：
 * 在 Thing 类中增加一个私有变量 arrayLis，类型为 ArrayList,然后通过 setValue 和 getValue 分别进
 * 行设置和取值，我们来看场景类：
 * </p>
 */
public class ShallowCopyClient {

    public static void main(String[] args) {

        // 产生一个对象
        ShallowCopy shallowCopy = new ShallowCopy();
        // 设置一个值
        shallowCopy.setValue("张三");

        // 拷贝一个对象
        ShallowCopy cloneShallowCopy = shallowCopy.clone();
        cloneShallowCopy.setValue("李四");

        // 运行的结果是：[张三, 李四]
        System.out.println(shallowCopy.getValue());

        /**
         * 怎么会有李四呢？是因为 Java 做了一个偷懒的拷贝动作，Object 类提供的方法 clone() 只是拷贝本对象，
         * 其对象内部的数组、引用对象等都不拷贝，还是指向原生对象的内部元素地址，这种拷贝就叫做浅拷贝，
         * 确实是非常浅，两个对象共享了一个私有变量，你改我改大家都能改，是一个种非常不安全的方式，在实
         * 际项目中使用还是比较少的。你可能会比较奇怪，为什么在 Mail 那个类中就可以使用使用 String 类型，
         * 而不会产生由浅拷贝带来的问题呢？内部的数组和引用对象才不拷贝，其他的原始类型比如
         * int,long,String(Java 就希望你把 String 认为是基本类型，String 是没有 clone() 方法的)等都会被拷贝的。
         */
    }
}
