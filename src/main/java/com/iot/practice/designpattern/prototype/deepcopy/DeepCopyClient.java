package com.iot.practice.designpattern.prototype.deepcopy;

/**
 * <p>ShallowCopyClient 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月06日 14:57</p>
 * <p>@remark：
 * 在 Thing 类中增加一个私有变量 arrayLis，类型为 ArrayList,然后通过 setValue 和 getValue 分别进
 * 行设置和取值，我们来看场景类：
 * </p>
 */
public class DeepCopyClient {

    public static void main(String[] args) {

        // 产生一个对象
        DeepCopy deepCopy = new DeepCopy();
        // 设置一个值
        deepCopy.setValue("张三");

        // 拷贝一个对象
        DeepCopy cloneDeepCopy = deepCopy.clone();
        cloneDeepCopy.setValue("李四");

        // 运行的结果是：[张三]
        System.out.println(deepCopy.getValue());

        /**
         * 这个实现了完全的拷贝，两个对象之间没有任何的瓜葛了，你修改你的，我修改我的，不相互影响，
         * 这种拷贝就叫做深拷贝，深拷贝还有一种实现方式就是通过自己写二进制流来操作对象，然后实现对象的
         * 深拷贝，这个大家有时间自己实现一下。
         * 深拷贝和浅拷贝建议不要混合使用，一个类中某些引用使用深拷贝某些引用使用浅拷贝，这是一种非
         * 常差的设计，特别是是在涉及到类的继承，父类有几个引用的情况就非常的复杂，建议的方案深拷贝和浅
         * 拷贝分开实现
         */
    }
}
