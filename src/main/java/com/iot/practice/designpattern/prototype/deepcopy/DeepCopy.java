package com.iot.practice.designpattern.prototype.deepcopy;

import java.util.ArrayList;

/**
 * <p>DeepCopy 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月06日 15:03</p>
 * <p>@remark：</p>
 */
public class DeepCopy implements Cloneable {

    /**
     * 定义一个私有变量
     */
    private ArrayList<String> arrayList = new ArrayList<>();

    @Override
    public DeepCopy clone() {
        DeepCopy deepCopy = null;

        try {
            deepCopy = (DeepCopy) super.clone();
            this.arrayList = (ArrayList<String>) this.arrayList.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        return deepCopy;
    }

    /**
     * 设置值
     */
    public void setValue(String value) {
        this.arrayList.add(value);
    }

    /**
     * 取值
     */
    public ArrayList<String> getValue() {
        return this.arrayList;
    }
}
