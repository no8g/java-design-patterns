package com.iot.practice.designpattern.proxy;

/**
 * <p>JiaShi 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年01月20日 18:10</p>
 * <p>@remark：</p>
 */
public class JiaShi implements KindWomen {
    @Override
    public void makeEyesWithMen() {
        System.out.println("贾氏抛媚眼");
    }

    @Override
    public void happyWithMen() {
        System.out.println("贾氏正在Happy中......");
    }
}
