package com.iot.practice.designpattern.proxy;

/**
 * <p>PanJinLian 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年01月20日 17:45</p>
 * <p>@remark：定义一个潘金莲是什么样的人</p>
 */
public class PanJinLian implements KindWomen {

    @Override
    public void makeEyesWithMen() {
        System.out.println("潘金莲抛媚眼");
    }

    @Override
    public void happyWithMen() {
        System.out.println("潘金莲在和男人做那个.....");
    }
}
