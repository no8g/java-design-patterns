package com.iot.practice.designpattern.proxy;

/**
 * <p>WangPo 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年01月20日 17:46</p>
 * <p>@remark：王婆这个人老聪明了，她太老了，是个男人都看不上，但是她有智慧有经验呀，她作为一类女人的代理！</p>
 */
public class WangPo implements KindWomen {

    private final KindWomen kindWomen;

    /**
     * 默认的话（无参构造函数），是潘金莲的代理
     */
    public WangPo() {
        this.kindWomen = new PanJinLian();
    }

    /**
     * 她可以是KindWomen的任何一个女人的代理，只要你是这一类型 （有参构造）
     */
    public WangPo(KindWomen kindWomen) {
        this.kindWomen = kindWomen;
    }

    /**
     * 自己老了，干不了，可以让年轻的代替
     */
    @Override
    public void makeEyesWithMen() {
        this.kindWomen.makeEyesWithMen();
    }

    /**
     * 王婆这么大年龄了，谁看她抛媚眼？！
     */
    @Override
    public void happyWithMen() {
        this.kindWomen.happyWithMen();
    }
}
