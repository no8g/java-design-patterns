package com.iot.practice.designpattern.proxy;

/**
 * <p>KindWomen 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年01月20日 17:42</p>
 * <p>@remark：定义一种类型的女人，王婆和潘金莲都属于这个类型的女人</p>
 */
public interface KindWomen {

    /**
     * 这种类型的女人能做什么事情呢？ 抛媚眼
     */
    void makeEyesWithMen();

    /**
     * happy what? You know that!
     */
    void happyWithMen();
}
