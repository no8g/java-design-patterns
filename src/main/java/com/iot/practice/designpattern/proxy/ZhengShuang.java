package com.iot.practice.designpattern.proxy;

/**
 * <p>ZhengShuang 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年01月20日 18:05</p>
 * <p>@remark：</p>
 */
public class ZhengShuang implements KindWomen{

    @Override
    public void makeEyesWithMen() {
        System.out.println("郑爽抛媚眼......");
    }

    @Override
    public void happyWithMen() {
        System.out.println("郑爽和那个男人在做......");
    }
}
