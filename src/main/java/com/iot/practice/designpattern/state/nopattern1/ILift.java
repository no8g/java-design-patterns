package com.iot.practice.designpattern.state.nopattern1;

/**
 * <p>ILift 此类用于：定义一个电梯的接口</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月19日 10:28</p>
 * <p>@remark：
 * 我们每天都在乘电梯，那我们来看看电梯有哪些动作（映射到 Java 中就是有多少方法）：开门、关门、
 * 运行、停止，就这四个动作，好，我们就用程序来实现一下电梯的动作
 * </p>
 */
public interface ILift {

    /**
     * 首先电梯门开启动作
     */
    public void open();

    /**
     * 电梯门有开启，那当然也就有关闭了
     */
    public void close();

    /**
     * 电梯要能上能下，跑起来
     */
    public void run();

    /**
     * 电梯还要能停下来，停不下来那就没人敢坐电梯了
     */
    public void stop();
}
