package com.iot.practice.designpattern.state.pattern;

/**
 * <p>OpenningState 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月22日 16:43</p>
 * <p>@remark：在电梯门开启的状态下能做什么事情</p>
 */
public class OpenningState extends LiftState {

    /**
     * 打开电梯门
     */
    @Override
    public void open() {
        System.out.println("电梯门开启...");
    }

    /**
     * 开启当然可以关闭了，我就想测试一下电梯门开关功能
     */
    @Override
    public void close() {
        // 状态修改
        super.context.setLiftState(Context.closingState);
        // 动作委托为CloseState来执行
        super.context.getLiftState().close();
    }

    /**
     * 门开着电梯就想跑，这电梯，吓死你！
     */
    @Override
    public void run() {
        // do nothing
    }

    @Override
    public void stop() {
        // do nothing
    }
}
