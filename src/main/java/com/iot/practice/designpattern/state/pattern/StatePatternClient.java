package com.iot.practice.designpattern.state.pattern;

/**
 * <p>StatePatternClient 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月25日 8:54</p>
 * <p>@remark：模拟电梯的动作</p>
 */
public class StatePatternClient {

    public static void main(String[] args) {
        Context context = new Context();
        context.setLiftState(new ClosingState());

        context.open();
        context.close();
        context.run();
        context.stop();
    }

    /**
     * 什么是状态模式呢？ 当一个对象内在状态改变时允许其改变行为，这个对象看起来像是改变了其类。
     * 说实话，这个定义的后半句我也没看懂，看过GOF才明白是怎么回事:
     * Allow an object to alter its behavior when its internal state changes.
     * The object will appear to change its class. [GoF, p305]，也就是说状态模式封装的非常好，状态的变更
     * 引起了行为的变更，从外部看起来就好像这个对象对应的类发生了改变一样
     */
}
