package com.iot.practice.designpattern.state.pattern;

/**
 * <p>StoppingState 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月22日 17:06</p>
 * <p>@remark：在停止状态下能做什么事情</p>
 */
public class StoppingState extends LiftState {

    /**
     * 停止状态，开门，那是肯定可以的！
     */
    @Override
    public void open() {
        super.context.setLiftState(Context.openningState);
        super.context.getLiftState().open();
    }

    /**
     * 停止状态关门？电梯门本来就是关着的！
     */
    @Override
    public void close() {
        // do nothing;
    }

    /**
     * 停止状态再跑起来，很正常的
     */
    @Override
    public void run() {
        super.context.setLiftState(Context.runningState);
        super.context.getLiftState().run();
    }

    /**
     * 停止状态是怎么发生的呢？当然是停止方法执行了
     */
    @Override
    public void stop() {
        System.out.println("电梯停止了...");
    }
}
