package com.iot.practice.designpattern.state.nopattern2;

/**
 * <p>ILift 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月19日 11:45</p>
 * <p>@remark：</p>
 */
public interface ILift {
    /**
     * 电梯的四个状态，1、开门状态；2、关闭状态；3、运行状态；4、停止状态
     */
    public static final int OPENING_STATE = 1;
    public static final int CLOSING_STATE = 2;
    public static final int RUNNING_STATE = 3;
    public static final int STOPPING_STATE = 4;

    /**
     * 设置电梯的状态
     *
     * @param state 状态
     */
    public void setState(int state);

    /**
     * 首先电梯门开启动作
     */
    public void open();

    /**
     * 电梯门有开启，那当然也就有关闭了
     */
    public void close();

    /**
     * 电梯要能上能下，跑起来
     */
    public void run();

    /**
     * 电梯还要能停下来，停不下来那就扯淡了
     */
    public void stop();
}
