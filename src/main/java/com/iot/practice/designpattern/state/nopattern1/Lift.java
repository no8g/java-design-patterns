package com.iot.practice.designpattern.state.nopattern1;

/**
 * <p>Lift 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月19日 10:32</p>
 * <p>@remark：</p>
 */
public class Lift implements ILift {

    @Override
    public void open() {
        System.out.println("电梯门开启...");
    }

    @Override
    public void close() {
        System.out.println("电梯门关闭...");
    }

    @Override
    public void run() {
        System.out.println("电梯上下跑起来...");
    }

    @Override
    public void stop() {
        System.out.println("电梯停止了...");
    }
}
