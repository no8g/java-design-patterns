package com.iot.practice.designpattern.state.nopattern2;

/**
 * <p>Lift 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月22日 15:25</p>
 * <p>@remark：程序有点长，但是还是很简单的，就是在每一个接口定义的方法中使用 witch…case 来进行判断，是否运行运行指定的动作。</p>
 */
public class Lift implements ILift {

    private int state;

    @Override
    public void setState(int state) {
        this.state = state;
    }

    /**
     * 电梯门开启
     */
    @Override
    public void open() {
        // 电梯在什么状态才能开启
        switch (this.state) {
            // 如果已经在门敞状态，则什么都不做
            case OPENING_STATE:
                // do nothing;
                break;
            // 如是电梯时关闭状态，则可以开启
            case CLOSING_STATE:
                this.openWithoutLogic();
                this.setState(OPENING_STATE);
                break;
            // 正在运行状态，则不能开门，什么都不做
            case RUNNING_STATE:
                // do nothing;
                break;
            // 停止状态，淡然要开门了
            case STOPPING_STATE:
                this.openWithoutLogic();
                this.setState(OPENING_STATE);
                break;
        }
    }

    /**
     * 电梯门关闭
     */
    @Override
    public void close() {
        // 电梯在什么状态下才能关闭
        switch (this.state) {
            // 如果是则可以关门，同时修改电梯状态
            case OPENING_STATE:
                this.closeWithoutLogic();
                this.setState(CLOSING_STATE);
                break;
            // 如果电梯就是关门状态，则什么都不做
            case CLOSING_STATE:
                //do nothing;
                break;
            // 如果是正在运行，门本来就是关闭的，也说明都不做
            case RUNNING_STATE:
                //do nothing;
                break;
            // 如果是停止状态，本也是关闭的，什么也不做
            case STOPPING_STATE:
                //do nothing;
                break;
        }
    }

    /**
     * 电梯开始跑起来
     */
    @Override
    public void run() {
        switch (this.state) {
            // 如果已经在门敞状态，则不你能运行，什么都不做
            case OPENING_STATE:
                //do nothing;
                break;
            // 如是电梯时关闭状态，则可以运行
            case CLOSING_STATE:
                this.runWithoutLogic();
                this.setState(RUNNING_STATE);
                break;
            // 正在运行状态，则什么都不做
            case RUNNING_STATE:
                //do nothing;
                break;
            // 停止状态，可以运行
            case STOPPING_STATE:
                this.runWithoutLogic();
                this.setState(RUNNING_STATE);
                break;
        }
    }

    @Override
    public void stop() {
        switch (this.state) {
            // 如果已经在门敞状态，那肯定要先停下来的，什么都不做
            case OPENING_STATE:
                //do nothing;
                break;
            // 如是电梯时关闭状态，则当然可以停止了
            case CLOSING_STATE:
                this.stopWithoutLogic();
                this.setState(CLOSING_STATE);
                break;
            // 正在运行状态，有运行当然那也就有停止了
            case RUNNING_STATE:
                this.stopWithoutLogic();
                this.setState(CLOSING_STATE);
                break;
            // 停止状态，什么都不做
            case STOPPING_STATE:
                //do nothing;
                break;
        }
    }

    /**
     * 纯粹的电梯关门，不考虑实际的逻辑
     */
    private void closeWithoutLogic() {
        System.out.println("电梯门关闭...");
    }

    /**
     * 纯粹的电梯门开，不考虑任何条件
     */
    private void openWithoutLogic() {
        System.out.println("电梯门开启...");
    }

    /**
     * 纯粹的运行，不考虑其他条件
     */
    private void runWithoutLogic() {
        System.out.println("电梯上下跑起来...");
    }

    /**
     * 单纯的停止，不考虑其他条件
     */
    private void stopWithoutLogic() {
        System.out.println("电梯停止了...");
    }
}
