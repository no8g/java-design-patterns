package com.iot.practice.designpattern.mediator.pattern;

/**
 * <p>SmallHouseColleague 此类用于：具体同事类1</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月12日 10:32</p>
 * <p>@remark：房东（小房间）</p>
 */
public class SmallHouseColleague extends Colleague{

    @Override
    public void handle() {
        System.out.println("单间 —— 便宜整洁");
    }
}
