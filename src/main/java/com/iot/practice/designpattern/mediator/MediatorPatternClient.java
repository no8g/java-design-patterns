package com.iot.practice.designpattern.mediator;

/**
 * <p>MediatorPatternClient 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月03日 15:57</p>
 * <p>@remark：</p>
 */
public class MediatorPatternClient {

    public static void main(String[] args) {
        User jack = new User("Jack");
        User rose = new User("Rose");

        jack.sendMessage("Hi, rose");
        rose.sendMessage("Hi, jack");

        jack.sendMessage("you jump, i jump");
        rose.sendMessage("ok, you follow me");
    }
}
