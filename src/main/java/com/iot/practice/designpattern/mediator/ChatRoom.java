package com.iot.practice.designpattern.mediator;

import java.util.Date;

/**
 * <p>ChatRoom 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月03日 15:52</p>
 * <p>@remark：</p>
 */
public class ChatRoom {

    public static void showMessage(User user, String message) {
        System.out.println(new Date().toString() + "【" + user.getName() + "】：" + message);
    }
}
