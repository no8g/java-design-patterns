package com.iot.practice.designpattern.mediator.pattern;

/**
 * <p>TwoHouseColleague 此类用于：具体同事类2</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月12日 10:33</p>
 * <p>@remark：房东（两居室）</p>
 */
public class TwoHouseColleague extends Colleague{

    /**
     * 展示两居室
     */
    @Override
    public void handle() {
        System.out.println("两居室 —— 合适，靠谱");
    }
}
