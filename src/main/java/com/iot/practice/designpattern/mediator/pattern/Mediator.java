package com.iot.practice.designpattern.mediator.pattern;

/**
 * <p>Mediator 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月12日 10:29</p>
 * <p>@remark：抽象中介者</p>
 */
public abstract class Mediator {

    /**
     * 定义与同事间的交互通信
     *
     * @param type
     */
    public abstract void common(String type);
}
