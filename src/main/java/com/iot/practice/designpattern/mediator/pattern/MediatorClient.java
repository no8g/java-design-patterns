package com.iot.practice.designpattern.mediator.pattern;

/**
 * <p>Client 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月12日 10:48</p>
 * <p>@remark：</p>
 */
public class MediatorClient {

    public static void main(String[] args) {

        System.out.println("一个租客看房： ");
        // 初始化中介
        HouseMediator mediator = new HouseMediator();

        // 初始化房屋信息
        SmallHouseColleague smallHouseColleague = new SmallHouseColleague( );
        TwoHouseColleague twoHouseColleague = new TwoHouseColleague( );
        ThreeHouseColleague threeHouseColleague = new ThreeHouseColleague( );

        // 中介获取房屋信息
        mediator.setSmallHouseColleague(smallHouseColleague);
        mediator.setTwoHouseColleague(twoHouseColleague);
        mediator.setThreeHouseColleague(threeHouseColleague);

        // 租户A需要两居室、提供看房
        System.out.println("------我要看两居室------");
        mediator.common("两居室");

        // 租户B需要四居室、暂无房源
        System.out.println("------我要看四居室------");
        mediator.common("四居室");
    }
}
