package com.iot.practice.designpattern.mediator.pattern;

/**
 * <p>Colleague 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月12日 10:31</p>
 * <p>@remark：抽象同事类</p>
 */
public abstract class Colleague {

    /**
     * 处理自己的事务（房东展示自己的房屋）
     */
    public abstract void handle();
}
