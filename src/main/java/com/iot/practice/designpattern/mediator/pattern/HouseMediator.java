package com.iot.practice.designpattern.mediator.pattern;

/**
 * <p>HouseMediator 此类用于：具体中介者</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月12日 10:32</p>
 * <p>@remark：房屋中介</p>
 */
public class HouseMediator extends Mediator {

    /**
     * 中介有所有房东的房屋信息
     */
    private SmallHouseColleague smallHouseColleague;
    private TwoHouseColleague twoHouseColleague;
    private ThreeHouseColleague threeHouseColleague;

    public void setSmallHouseColleague(SmallHouseColleague smallHouseColleague) {
        this.smallHouseColleague = smallHouseColleague;
    }

    public void setTwoHouseColleague(TwoHouseColleague twoHouseColleague) {
        this.twoHouseColleague = twoHouseColleague;
    }

    public void setThreeHouseColleague(ThreeHouseColleague threeHouseColleague) {
        this.threeHouseColleague = threeHouseColleague;
    }

    @Override
    public void common(String type) {
        switch (type) {
            case "单间":
                smallHouseColleague.handle();
                System.out.println("如果可以就可以租房了!");
                break;
            case "两居室":
                twoHouseColleague.handle();
                System.out.println("如果可以就可以租房了!");
                break;
            case "三居室":
                threeHouseColleague.handle();
                System.out.println("如果可以就可以租房了!");
                break;
            default:
                System.out.println(type + "暂时没有房源!");
                break;
        }
    }
}
