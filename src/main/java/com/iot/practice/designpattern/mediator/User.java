package com.iot.practice.designpattern.mediator;

/**
 * <p>User 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月03日 15:54</p>
 * <p>@remark：</p>
 */
public class User {

    private String name;

    public User(String name) {
        this.name = name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void sendMessage(String message) {
        ChatRoom.showMessage(this, message);
    }
}
