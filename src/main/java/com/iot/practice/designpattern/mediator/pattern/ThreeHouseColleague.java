package com.iot.practice.designpattern.mediator.pattern;

/**
 * <p>ThreeHouseColleague 此类用于：具体同事类3</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月12日 10:34</p>
 * <p>@remark：房东（三居室）</p>
 */
public class ThreeHouseColleague extends Colleague{

    /**
     * 展示三居室
     */
    @Override
    public void handle() {
        System.out.println("三居室 —— 大气，宽松");
    }
}
