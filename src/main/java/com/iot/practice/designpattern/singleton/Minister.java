package com.iot.practice.designpattern.singleton;

/**
 * <p>Minister 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年01月22日 17:21</p>
 * <p>@remark：</p>
 */
public class Minister {

    public static void main(String[] args) {

        Emperor emperor1 = Emperor.getInstance();
        emperor1.emperorInfo();

        Emperor emperor2 = Emperor.getInstance();
        emperor2.emperorInfo();

        Emperor emperor3 = Emperor.getInstance();
        emperor3.emperorInfo();

        System.out.println(emperor1 == emperor2);
        System.out.println(emperor2 == emperor3);
    }
}
