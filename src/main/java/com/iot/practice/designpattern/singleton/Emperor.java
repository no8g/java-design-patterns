package com.iot.practice.designpattern.singleton;

/**
 * <p>Emperor 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年01月22日 16:47</p>
 * <p>@remark：</p>
 */
public class Emperor {

    /**
     * 定义一个皇帝放在那里，然后给这个皇帝名字
     */
    private static Emperor emperor = null;

    private Emperor(){
        // 世俗和道德约束你，目的就是不让你产生第二个皇帝.....
    }

    public static Emperor getInstance() {
        if (emperor == null) {
            emperor = new Emperor();
        }

        return emperor;
    }

    public void emperorInfo() {
        System.out.println("I am the Emperor called WangWu.......");
    }
}
