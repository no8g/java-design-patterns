package com.iot.practice.designpattern.builder.pattern;

/**
 * <p>Client 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月07日 10:51</p>
 * <p>@remark：这里是牛叉公司的天下，他要啥我们给啥</p>
 */
public class Client {

    public static void main(String[] args) {

        BuilderDirector builderDirector = new BuilderDirector();

        for (int i = 0; i < 10; i++) {
            builderDirector.getBenzAModel().run();
        }

        for (int i = 0; i < 10; i++) {
            builderDirector.getBenzBModel().run();
        }

        for (int i = 0; i < 10; i++) {
            builderDirector.getBMWCModel().run();
        }

        for (int i = 0; i < 10; i++) {
            builderDirector.getBMWDModel().run();
        }
    }
}
