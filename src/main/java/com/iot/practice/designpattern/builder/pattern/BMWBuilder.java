package com.iot.practice.designpattern.builder.pattern;

import com.iot.practice.designpattern.builder.BMWModel;
import com.iot.practice.designpattern.builder.CarModel;

import java.util.List;

/**
 * <p>BMWBuilder 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月07日 9:53</p>
 * <p>@remark：给定一个顺序，返回一个宝马车</p>
 */
public class BMWBuilder extends CarBuilder{

    private BMWModel bmwModel = new BMWModel();

    @Override
    public void setSequence(List<String> sequence) {
        this.bmwModel.setSequenceList(sequence);
    }

    @Override
    public CarModel getCarModel() {
        return this.bmwModel;
    }
}
