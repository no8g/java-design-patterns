package com.iot.practice.designpattern.builder;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>Client 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月07日 9:17</p>
 * <p>@remark：最终客户开始使用这个模型</p>
 */
public class BuilderClient {

    public static void main(String[] args) {

        // 客户告诉牛叉公司，我要这样一个模型，然后牛叉公司就告诉我老大，说要这样一个模型，这样一个顺序，然后我就来制造
        BenzModel benzModel = new BenzModel();

        List<String> sequenceList = new ArrayList<>();

        // 客户要求，run的时候时候先发动引擎
        sequenceList.add("engine boom");
        // 启动起来
        sequenceList.add("start");
        // 开了一段就停下来
        sequenceList.add("stop");

        // 然后我们把这个顺序给奔驰车：
        benzModel.setSequenceList(sequenceList);
        benzModel.run();

        BMWModel bmwModel = new BMWModel();
        // 然后我们把这个顺序给宝马车：
        bmwModel.setSequenceList(sequenceList);
        bmwModel.run();
    }
}
