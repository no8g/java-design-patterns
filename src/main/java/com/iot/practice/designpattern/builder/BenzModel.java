package com.iot.practice.designpattern.builder;

/**
 * <p>BenzModel 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月07日 9:14</p>
 * <p>@remark：奔驰车模型</p>
 */
public class BenzModel extends CarModel {

    @Override
    protected void start() {
        System.out.println("奔驰车跑起来是这个样子的...");
    }

    @Override
    protected void stop() {
        System.out.println("奔驰车应该这样停车...");
    }

    @Override
    protected void alarm() {
        System.out.println("奔驰车的喇叭声音是这个样子的...");
    }

    @Override
    protected void engineBoom() {
        System.out.println("奔驰车的引擎是这个声音的...");
    }
}
