package com.iot.practice.designpattern.builder;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>CarModel 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月07日 8:45</p>
 * <p>@remark：定义一个车辆模型的抽象类，所有的车辆模型都继承这里类</p>
 */
public abstract class CarModel {

    /**
     * /这个参数是各个基本方法执行的顺序
     */
    private List<String> sequenceList = new ArrayList<>();

    /**
     * 模型是启动开始跑了
     */
    protected abstract void start();

    /**
     * 能发动，那还要能停下来，那才是真本事
     */
    protected abstract void stop();

    /**
     * 喇叭会出声音，是滴滴叫，还是哔哔叫
     */
    protected abstract void alarm();

    /**
     * 引擎会轰隆隆的响，不响那是假的
     */
    protected abstract void engineBoom();

    /**
     * 那模型应该会跑吧，别管是人推的，还是电力驱动，总之要会跑
     */
    final public  void run() {
        // 循环一遍，谁在前，就先执行谁
        for (int i = 0; i < this.sequenceList.size(); i++) {
            String actionName = this.sequenceList.get(i);
            if ("start".equals(actionName)) {
                // 如果是 start 关键字，就开启汽车
                this.start();
            } else if ("stop".equals(actionName)) {
                // 如果是 stop 关键字，就停止汽车
                this.stop();
            } else if ("alarm".equals(actionName)) {
                // 如果是 alarm 关键字，喇叭就开始叫了
                this.alarm();
            } else if ("engine boom".equals(actionName)) {
                // 如果是 engine boom 关键字，引擎就开始轰鸣
                this.engineBoom();
            }
        }
    }

    /**
     * 把传递过来的值传递到类内
     *
     * @param sequenceList 各个基本方法执行的顺序的集合
     */
    final public void setSequenceList(List<String> sequenceList) {
        this.sequenceList = sequenceList;
    }
}
