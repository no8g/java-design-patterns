package com.iot.practice.designpattern.builder.pattern;

import com.iot.practice.designpattern.builder.BMWModel;
import com.iot.practice.designpattern.builder.BenzModel;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>BuilderDirector 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月07日 10:12</p>
 * <p>@remark：导演安排顺序，生产车辆模型</p>
 */
public class BuilderDirector {

    private List<String> sequenceList = new ArrayList<>();

    private BenzBuilder benzBuilder = new BenzBuilder();

    private BMWBuilder bmwBuilder = new BMWBuilder();

    /**
     * A类型的奔驰车模型，先start,然后stop,其他什么引擎了，喇叭一概没有
     *
     * @return A类型的奔驰车模型
     */
    public BenzModel getBenzAModel() {
        // 清理场景，这里是一些初级程序员不注意的地方
        this.sequenceList.clear();

        // 这只BenzAModel的执行顺序
        this.sequenceList.add("start");
        this.sequenceList.add("stop");

        this.benzBuilder.setSequence(sequenceList);
        return (BenzModel) this.benzBuilder.getCarModel();
    }

    /**
     * B型号的奔驰车模型，是先发动引擎，然后启动，然后停止，没有喇叭
     *
     * @return B型号的奔驰车模型
     */
    public BenzModel getBenzBModel() {
        // 清理场景，这里是一些初级程序员不注意的地方
        this.sequenceList.clear();

        // 这只BenzBModel的执行顺序
        this.sequenceList.add("engine boom");
        this.sequenceList.add("start");
        this.sequenceList.add("stop");

        this.benzBuilder.setSequence(sequenceList);
        return (BenzModel) this.benzBuilder.getCarModel();
    }

    /**
     * C型号的宝马车是先按下喇叭（炫耀嘛），然后启动，然后停止
     *
     * @return C型号的宝马车
     */
    public BMWModel getBMWCModel() {
        this.sequenceList.clear();

        this.sequenceList.add("alarm");
        this.sequenceList.add("start");
        this.sequenceList.add("stop");

        this.bmwBuilder.setSequence(sequenceList);
        return (BMWModel) this.bmwBuilder.getCarModel();
    }

    /**
     * D类型的宝马车只有一个功能，就是跑，启动起来就跑，永远不停止，牛叉
     *
     * @return D型号的宝马车
     */
    public BMWModel getBMWDModel() {
        this.sequenceList.clear();

        this.sequenceList.add("start");

        this.bmwBuilder.setSequence(sequenceList);
        return (BMWModel) this.bmwBuilder.getCarModel();
    }

    /**
     * 这里还可以有很多方法，你可以先停止，然后再启动，或者一直停着不动，静态的嘛
     * 导演类嘛，按照什么顺序是导演说了算
     */
}
