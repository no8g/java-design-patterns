package com.iot.practice.designpattern.builder.pattern;

import com.iot.practice.designpattern.builder.CarModel;

import java.util.List;

/**
 * <p>CarBuilder 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月07日 9:40</p>
 * <p>@remark：要什么顺序的车，你说，我给建造出来</p>
 */
public abstract class CarBuilder {

    /**
     * 建造一个模型，你要给我一个顺序要，就是组装顺序
     *
     * @param sequence 组装顺序
     */
    public abstract void setSequence(List<String> sequence);

    /**
     * 设置完毕顺序后，就可以直接拿到这个车辆模型
     *
     * @return 车辆模型
     */
    public abstract CarModel getCarModel();
}
