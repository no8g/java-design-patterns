package com.iot.practice.designpattern.builder.pattern;

import com.iot.practice.designpattern.builder.BenzModel;
import com.iot.practice.designpattern.builder.CarModel;

import java.util.List;

/**
 * <p>BenzBuilder 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年02月07日 9:49</p>
 * <p>@remark：各种设施都给了，我们按照一定的顺序制造一个奔驰车</p>
 */
public class BenzBuilder extends CarBuilder{

    private BenzModel benzModel = new BenzModel();

    @Override
    public void setSequence(List<String> sequence) {
        this.benzModel.setSequenceList(sequence);
    }

    @Override
    public CarModel getCarModel() {
        return this.benzModel;
    }
}
