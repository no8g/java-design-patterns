package com.iot.practice.designpattern.strategy;

/**
 * <p>IStrategy 此接口用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月26日 14:53</p>
 * <p>@remark：首先定一个策略接口，这是诸葛亮老人家给赵云的三个锦囊妙计的接口</p>
 */
public interface IStrategy {

    /**
     * 每个锦囊妙计都是一个可执行的算法
     */
    public void operation();
}
