package com.iot.practice.designpattern.strategy;

/**
 * <p>First 此类用于：找乔国老帮忙，使孙权不能杀刘备</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月26日 14:54</p>
 * <p>@remark：然后再写三个实现类，有三个妙计嘛</p>
 */
public class First implements IStrategy {

    @Override
    public void operation() {
        System.out.println("找乔国老帮忙，让吴国太给孙权施加压力");
    }
}
