package com.iot.practice.designpattern.strategy;

/**
 * <p>Third 此类用于：孙夫人断后，挡住追兵</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月26日 14:56</p>
 * <p>@remark：</p>
 */
public class Third implements IStrategy {

    @Override
    public void operation() {
        System.out.println("孙夫人断后，挡住追兵");
    }
}
