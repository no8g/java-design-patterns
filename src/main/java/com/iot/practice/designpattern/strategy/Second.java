package com.iot.practice.designpattern.strategy;

/**
 * <p>Second 此类用于：求吴国太开个绿灯</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月26日 14:55</p>
 * <p>@remark：</p>
 */
public class Second implements IStrategy {

    @Override
    public void operation() {
        System.out.println("求吴国太开个绿灯,放行！ ");
    }
}
