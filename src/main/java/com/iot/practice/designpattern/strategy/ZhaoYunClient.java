package com.iot.practice.designpattern.strategy;

/**
 * <p>StrategyClient 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月26日 14:57</p>
 * <p>@remark：赵云出场了，他根据诸葛亮给他的交代，依次拆开妙计</p>
 */
public class ZhaoYunClient {

    public static void main(String[] args) {

        StrategyContext context;

        // 刚刚到吴国的时候拆第一个
        System.out.println("-----------刚刚到吴国的时候拆第一个-------------");
        context = new StrategyContext(new First());
        context.operation();
        System.out.println("\n");

        //刘备乐不思蜀了，拆第二个了
        System.out.println("-----------刘备乐不思蜀了，拆第二个了-------------");
        context = new StrategyContext(new Second());
        context.operation();
        System.out.println("\n");

        //孙权的小兵追了，咋办？拆第三个
        System.out.println("-----------孙权的小兵追来了，咋办？拆第三个-------------");
        context = new StrategyContext(new Third());
        context.operation();
        System.out.println("\n");
    }
}
