package com.iot.practice.designpattern.strategy;

/**
 * <p>StrategyContext 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年03月26日 14:57</p>
 * <p>@remark：计谋有了，那还要有锦囊</p>
 */
public class StrategyContext {

    private IStrategy iStrategy;

    /**
     * 构造函数，你要使用那个妙计
     *
     * @param iStrategy 妙计接口入参
     */
    public StrategyContext(IStrategy iStrategy) {
        this.iStrategy = iStrategy;
    }

    /**
     * 使用计谋了，看我出招了
     */
    public void operation() {
        this.iStrategy.operation();
    }
}
