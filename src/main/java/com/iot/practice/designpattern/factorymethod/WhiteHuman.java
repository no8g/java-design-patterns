package com.iot.practice.designpattern.factorymethod;

/**
 * <p>WhiteHuman 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年01月26日 17:50</p>
 * <p>@remark：白色人种</p>
 */
public class WhiteHuman implements Human {

    @Override
    public void laugh() {
        System.out.println("白色人种会大笑，侵略的笑声");
    }

    @Override
    public void cry() {
        System.out.println("白色人种会哭");
    }

    @Override
    public void talk() {
        System.out.println("白色人种会说话，一般都是但是单字节！");
    }
}
