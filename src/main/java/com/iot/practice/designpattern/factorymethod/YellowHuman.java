package com.iot.practice.designpattern.factorymethod;

/**
 * <p>YellowHuman 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年01月26日 18:06</p>
 * <p>@remark：黄色人种，这个翻译的不准确，将就点吧</p>
 */
public class YellowHuman implements Human{

    @Override
    public void laugh() {
        System.out.println("黄色人种会大笑，幸福呀！ ");
    }

    @Override
    public void cry() {
        System.out.println("黄色人种会哭");
    }

    @Override
    public void talk() {
        System.out.println("黄色人种会说话，一般说的都是双字节");
    }
}
