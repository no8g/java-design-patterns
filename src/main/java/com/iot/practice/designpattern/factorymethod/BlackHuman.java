package com.iot.practice.designpattern.factorymethod;

/**
 * <p>BlackHuman 此类用于：</p>
 * <p>@author：hujm</p>
 * <p>@date：2021年01月26日 18:08</p>
 * <p>@remark：黑色人种，记得中学学英语，老师说black man是侮辱人的意思，不懂，没跟老外说话</p>
 */
public class BlackHuman implements Human{

    @Override
    public void laugh() {
        System.out.println("黑人会笑");
    }

    @Override
    public void cry() {
        System.out.println("黑人会哭");
    }

    @Override
    public void talk() {
        System.out.println("黑人可以说话，一般人听不懂");
    }
}
